package dk.kea.class2016february.cristianaman.gameengine;

import dk.kea.class2016february.cristianaman.gameengine.BreakoutGame.GameScreen;
import dk.kea.class2016february.cristianaman.gameengine.BreakoutGame.MainMenuScreen;

/**
 * Created by mancr on 17/02/2016.
 */
public class SimpleGame extends Game
{
    @Override
    public Screen createStartScreen()
    {
        return new MainMenuScreen(this);
    }
}