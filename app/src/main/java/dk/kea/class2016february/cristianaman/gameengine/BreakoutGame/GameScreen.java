package dk.kea.class2016february.cristianaman.gameengine.BreakoutGame;

import android.graphics.Bitmap;

import dk.kea.class2016february.cristianaman.gameengine.Game;
import dk.kea.class2016february.cristianaman.gameengine.Screen;

/**
 * Created by mancr on 11/04/2016.
 */
public class GameScreen extends Screen
{
    enum State
    {
        Paused,
        Running,
        GameOver
    }

    Bitmap backround;
    Bitmap resume;
    Bitmap gameOver;

    World world;
    WorldRenderer renderer;

    State state = State.Running;

    public GameScreen(Game game)
    {
        super(game);
        backround = game.loadBitmap("background.png");
        resume = game.loadBitmap("resume.png");
        gameOver = game.loadBitmap("gameover.png");
        world = new World();
        renderer = new WorldRenderer(game, world);
    }

    @Override
    public void update(float deltaTime)
    {
        // set state on touch
        if (state == State.Running && game.getTouchEvents().size() > 0) // if touched when running
        {
            state = state.Paused; // pause
        }
        if (state == State.Paused && game.getTouchEvents().size() > 0) // if touched when paused
        {
            state = State.Running; // unpause
        }
        if (state == State.GameOver && game.getTouchEvents().size() > 0) // if game over
        {
            game.setScreen(new MainMenuScreen(game)); // start new game
            return;
        }

        // draw depending on state
        game.drawBitmap(backround, 0, 0);

        if (state == State.Paused)
        {
            game.drawBitmap(resume, 160 - resume.getWidth() / 2, 240 - resume.getHeight() / 2);
        }
        if (state == State.GameOver)
        {
            game.drawBitmap(gameOver, 160 - gameOver.getWidth() / 2, 240 - gameOver.getHeight() / 2);
        }
        // do something with ball
        if (state == State.Running)
        {
            int touchX = -1;
            if (game.isTouchDown(0)) touchX = game.getTouchX(0);
            world.update(deltaTime, game.getAccelerometer()[1], touchX);
        }
        game.drawBitmap(backround,0,0);
        renderer.render();

    }

    @Override
    public void pause()
    {
        if (state == State.Running)
        {
            state = State.Paused;
        }
    }

    @Override
    public void resume()
    {

    }

    @Override
    public void dispose()
    {

    }
}
