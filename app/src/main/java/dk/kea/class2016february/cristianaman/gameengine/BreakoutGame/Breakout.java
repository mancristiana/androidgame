package dk.kea.class2016february.cristianaman.gameengine.BreakoutGame;

import dk.kea.class2016february.cristianaman.gameengine.Game;
import dk.kea.class2016february.cristianaman.gameengine.Screen;

/**
 * Created by mancr on 11/04/2016.
 */
public class Breakout extends Game
{
    @Override
    public Screen createStartScreen()
    {
        return new MainMenuScreen(this);
    }
}
