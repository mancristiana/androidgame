package dk.kea.class2016february.cristianaman.gameengine;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.Random;

/**
 * Created by mancr on 17/02/2016.
 */
public class SimpleScreen extends Screen
{
    Bitmap bob;
    float x = 0;
    int y = 0;
    Random random = new Random();
    int clearColor = random.nextInt();
    Sound sound;
    Music music;
    boolean userWantsMusic = false;


    public SimpleScreen(Game game)
    {
        super(game);
        bob = game.loadBitmap("bob.png");
        sound = game.loadSound("explosion.ogg");
        music = game.loadMusic("music.ogg");
    }

    @Override
    public void update(float deltaTime)
    {
//        Log.d("Framerate","fps: " + game.getFramerate() + "*********************************");
        game.clearFramebuffer(clearColor);

        x = x + 50 * deltaTime;
        if(x > game.getOffscreenHeight()) x = -50;
        game.drawBitmap(bob, (int) x, 10);


        float x = -game.getAccelerometer()[0];
        float y = game.getAccelerometer()[1];
        x = (x / 10) * game.getOffscreenWidth() / 2 + game.getOffscreenWidth() / 2;
        y = (y / 10) * game.getOffscreenHeight() / 2 + game.getOffscreenHeight() / 2;

        game.drawBitmap(bob, (int) x - 64, (int) y - 64);

        if(game.isTouchDown(0))
        {
            if(userWantsMusic)
            {
                music.pause();
                userWantsMusic = false;
            } else
            {
                music.play();
                userWantsMusic = true;
            }
            sound.play(1);
        }


//        for (int pointer = 0; pointer < 5; pointer++)
//        {
//            if (game.isTouchDown(pointer))
//            {
//                game.drawBitmap(bitmap, game.getTouchX(pointer), game.getTouchY(pointer));
//            }
//        }
//
//        game.drawBitmap(bitmap, 10, 10);
//        game.drawBitmap(bitmap, 100, 140, 0, 0, 64, 64);
//
//        if(game.isKeyPressed(MyKeyEvent.KEYCODE_MENU))
//        {
//            clearColor = random.nextInt();
//        }
    }

    @Override
    public void pause()
    {
        music.pause();
        Log.d("SimpleScreen", "We are pausing!");
    }

    @Override
    public void resume()
    {
        if (userWantsMusic) music.play();
        Log.d("SimpleScreen", "We are resuming!");
    }

    @Override
    public void dispose()
    {
        Log.d("SimpleScreen", "We are disposing the game!");
    }

}
