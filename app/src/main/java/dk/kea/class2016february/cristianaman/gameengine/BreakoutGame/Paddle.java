package dk.kea.class2016february.cristianaman.gameengine.BreakoutGame;

/**
 * Created by mancr on 11/04/2016.
 */
public class Paddle
{
    public static final float WIDTH = 56;
    public static final float HEIGHT = 11;
    public float x = 160 - WIDTH/2;
    public float y = World.MAX_Y - 30;
}
