package dk.kea.class2016february.cristianaman.gameengine.BreakoutGame;

import android.graphics.Bitmap;

import dk.kea.class2016february.cristianaman.gameengine.Game;
import dk.kea.class2016february.cristianaman.gameengine.Screen;

/**
 * Created by mancr on 11/04/2016.
 */
public class MainMenuScreen extends Screen
{
    Bitmap mainMenuImage;
    Bitmap insertCoinImage;
    float passedtime = 0;

    public MainMenuScreen(Game game)
    {
        super(game);
        mainMenuImage = game.loadBitmap("mainmenu.png");
        insertCoinImage = game.loadBitmap("insertcoin.png");
    }

    @Override
    public void update(float deltaTime)
    {

        if (game.isTouchDown(0))
        {
            game.setScreen(new GameScreen(game));
            return;
        }
        passedtime = passedtime + deltaTime;
        game.drawBitmap(mainMenuImage, 0, 0);
        if ((passedtime - (int) passedtime) > 0.5f)
        {
            game.drawBitmap(insertCoinImage, 160 - insertCoinImage.getWidth()/2, 320);
        }
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void dispose()
    {

    }
}
