package dk.kea.class2016february.cristianaman.gameengine.BreakoutGame;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import dk.kea.class2016february.cristianaman.gameengine.Game;

/**
 * Created by mancr on 11/04/2016.
 */
public class World
{
    public static final float MIN_X = 0;
    public static final float MAX_X = 319;
    public static final float MIN_Y = 32;
    public static final float MAX_Y = 479;

    Ball ball = new Ball();
    Paddle paddle = new Paddle();

    List<Block> blocks = new ArrayList<>();

    public World()
    {
        generateBlocks();
    }

    private void generateBlocks()
    {
        blocks.clear();

        int type = 0;
        int startY = (int) (MIN_Y + Ball.HEIGHT * 2);
        for (int y = startY; y < startY + 8 * Block.HEIGHT; y += (int) Block.HEIGHT, type++)
            for (int x = 20; x < 320 - Block.WIDTH / 2; x += (int) Block.WIDTH)
                blocks.add(new Block(x, y, type));
    }

    public void update(float deltaTime, float accelX, float touchX)
    {
        ball.x = ball.x + ball.velocityX * deltaTime;
        ball.y = ball.y + ball.velocityY * deltaTime;
        collideWalls();

        paddle.x = paddle.x - (accelX * 50 * deltaTime);
        if (touchX > 0) paddle.x = touchX - (int) Paddle.WIDTH / 2;
        if (paddle.x < MIN_X)
            paddle.x = MIN_X;
        if (paddle.x + Paddle.WIDTH > MAX_X)
            paddle.x = MAX_X - Paddle.WIDTH;

        collideBallPaddle();
        collideBallBlocks(deltaTime);

    }

    private void collideWalls()
    {
        if (ball.x < MIN_X)
        {
            ball.velocityX = -ball.velocityX;
            ball.x = MIN_X;
        } else if (ball.x + Ball.WIDTH > MAX_X)
        {
            ball.velocityX = -ball.velocityX;
            ball.x = MAX_X - Ball.WIDTH;
        }
        if (ball.y < MIN_Y)
        {
            ball.velocityY = -ball.velocityY;
            ball.y = MIN_Y;
        } else if (ball.y + Ball.HEIGHT > MAX_Y)
        {
            ball.velocityY = -ball.velocityY;
            ball.y = MAX_Y - Ball.HEIGHT; // change to min_y for
        }

    }

    private void collideBallPaddle()
    {
        if (ball.y > paddle.y) return;
        if (ball.x + Ball.WIDTH >= paddle.x &&
                ball.x < paddle.x + Paddle.WIDTH &&
                ball.y + Ball.HEIGHT + 2 >= paddle.y)
        {
            ball.y = paddle.y - Ball.HEIGHT;
            ball.velocityY = -ball.velocityY;
        }
    }

    private void collideBallBlocks(float deltaTime)
    {
        Block block;
        int stop = blocks.size();
        for (int i = 0; i < stop; i++)
        {
            block = blocks.get(i);
            if (collideRects(ball.x, ball.y, Ball.WIDTH, Ball.HEIGHT, block.x, block.y, Block.WIDTH, Block.HEIGHT))
            {
                blocks.remove(i);
                i--;
                stop--;

                float oldvx = ball.velocityX;
                float oldvy = ball.velocityY;

                reflectBall(ball, block);

                ball.x = ball.x - oldvx * deltaTime * 1.01f;
                ball.y = ball.y - oldvy * deltaTime * 1.01f;
            }
        }
    }

    private boolean collideRects(float x, float y, float width, float height,
                                 float x2, float y2, float width2, float height2)
    {
        if (x < x2 + width2 &&
                x + width > x2 &&
                y < y2 + height2 &&
                y + height > y2)
            return true;
        return false;
    }

    private void reflectBall(Ball ball, Block block)
    {
        if (collideRects(ball.x, ball.y, Ball.WIDTH, Ball.HEIGHT, block.x, block.y, 1, 1)) // check top left corner
        {
            if (ball.velocityX > 0) ball.velocityX = -ball.velocityX;
            if (ball.velocityY > 0) ball.velocityY = -ball.velocityY;
        }
    }
}
