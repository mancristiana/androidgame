package dk.kea.class2016february.cristianaman.gameengine;

/**
 * Created by mancr on 17/02/2016.
 */
public enum State
{
    Running,
    Paused,
    Resumed,
    Disposed
}
